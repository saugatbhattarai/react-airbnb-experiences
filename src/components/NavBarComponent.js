
export default function NavBarComponent(){
    return(
        <div className="navbar">
            <div className="navbar-logo">
                <img src="./images/airbnb_logo.png" className="airbnb-logo" alt="airbnb logo"/>
            </div>
        </div>
    );
}