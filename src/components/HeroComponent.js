export default function HeroComponent() {
  return (
    <section className="hero-section">
      <div className="hero-images">
        <img
          src="./images/grid_images.png"
          className="grid-images"
          alt="grid images/photos"
        />
      </div>
      <div className="hero-content">
        <h1>Online Experiences</h1>
        <p> Join unique interaction activities led by 
            one-of-a-kind hosts-all without leaving home.
        </p>
      </div>
    </section>
  );
}
