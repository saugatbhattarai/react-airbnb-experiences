export default [
  {
    id: 1,
    title: "Life Lessons With Katie Zaferes",
    description:
      "Intered in becoming a wedding photographer? For beginner and experience photographers AnimationPlaybackEvent, join us in learning techniques required to leave the happy",
    price: 136,
    coverImg: "./images/image1.png",
    stats: {
      rating: 5.0,
      reviewCount: 6,
    },
    location: "USA",
    openSpots: 0,
  },
  {
    id: 2,
    title: "Learn Wedding Photography",
    description:
      "Intered in becoming a wedding photographer? For beginner and experience photographers AnimationPlaybackEvent, join us in learning techniques required to leave the happy",
    price: 125,
    coverImg: "./images/image3.jpeg",
    stats: {
      rating: 5.0,
      reviewCount: 30,
    },
    location: "Online",
    openSpots: 27,
  },
  {
    id: 3,
    title: "Group Mountain Biking",
    description:
      "Intered in becoming a wedding photographer? For beginner and experience photographers AnimationPlaybackEvent, join us in learning techniques required to leave the happy",
    price: 50,
    coverImg: "./images/image2.jpeg",
    stats: {
      rating: 5.0,
      reviewCount: 30,
    },
    location: "Norway",
    openSpots: 3,
  },  
  {
    id: 4,
    title: "Group Mountain Biking",
    description:
      "Intered in becoming a wedding photographer? For beginner and experience photographers AnimationPlaybackEvent, join us in learning techniques required to leave the happy",
    price: 50,
    coverImg: "./images/image2.jpeg",
    stats: {
      rating: 5.0,
      reviewCount: 30,
    },
    location: "Norway",
    openSpots: 3,
  },
  {
    id: 5,
    title: "Group Mountain Biking",
    description:
      "Intered in becoming a wedding photographer? For beginner and experience photographers AnimationPlaybackEvent, join us in learning techniques required to leave the happy",
    price: 50,
    coverImg: "./images/image2.jpeg",
    stats: {
      rating: 5.0,
      reviewCount: 30,
    },
    location: "Norway",
    openSpots: 3,
  },
];
