import NavBarComponent from "./components/NavBarComponent";
import MainComponent from "./components/MainComponent";
import HeroComponent from "./components/HeroComponent";
import data from "./data";

import "./App.css";

function App() {
  const experienceCard = data.map(experience => 
      <MainComponent
          key={experience.id}
          experience={experience}
          // image={experience.coverImg}
          // rating={experience.stats.rating}
          // reviewCount={experience.stats.reviewCount}
          // location={experience.location}
          // title={experience.title}
          // price={experience.price}
          // openSpots={experience.openSpots}
      />
    )
  return (
    <div className="container">
      <NavBarComponent />
      <HeroComponent />
      <section className="main-content">
          {experienceCard}
      </section>
    </div>
  );
}

export default App;
